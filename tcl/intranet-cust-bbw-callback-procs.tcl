ad_proc -public -callback im_event_participant_after_create -impl bbw_send_welcome_email {
    {-object_id:required}
} {
    Sending welcome email
} {
    ns_log Debug "Executing im_event_participant_after_create CALLBACK bbw_send_welcome_email"
    
    set body ""
    set participant_id $object_id
    
    # Participant (person), Event (project) info query
    db_0or1row get_participant_info "select ep.person_id, ep.participant_id, ep.project_id, ep.party_pass_interested_p, ep.event_participant_type_id, ep.event_participant_status_id, u.email, u.first_names, u.last_name, u.user_id, ep.course as course_id, ep.food_choice as food_choice_id, ep.event_partners_text from im_event_participants ep, cc_users u where u.user_id = ep.person_id and ep.participant_id=:participant_id"
    
    db_0or1row get_event_info "select event_email from event_management_events where project_id = :project_id"

    set event_participant_status_name [im_name_from_id $event_participant_status_id]
    set locale [lang::user::locale -user_id $user_id]
    set token [im_generate_auto_login -user_id $user_id]
    set sencha_registration_app_url [parameter::get_from_package_key -package_key "intranet-event-management" -parameter "SenchaRegistrationAppUrl" -default ""]
    set link "$sencha_registration_app_url/?page=waiting-spot&ws_token=$token&ws_user_id=$user_id"

    # Check if lottery was run
    set waiting_spot_info ""
    if {[im_dynfield::attribute::exists_p -object_type event_management_events -attribute_name lottery_run_p]} {
        set lottery_run_p [db_string check_lottery "select 1 from event_management_events where project_id=:project_id and lottery_run_p = '1'" -default 0]
        if {$lottery_run_p eq 1} {
            set waiting_spot [event_management_participant_waiting_spot -participant_id $participant_id]
            set waiting_spot_info [lang::message::lookup $locale intranet-cust-bbw.waiting_spot_info ""]
        } 
    }

    # We get event_name which is project_name from im_projects 
    set event_name [im_name_from_id $project_id]    
    set subject [lang::message::lookup $locale intranet-cust-bbw.welcome_email_subject "$event_name - Thank you for registering"]

    set registration_info ""

    if {[exists_and_not_null course_id]} {
        set course_name [im_material_name -material_id $course_id]
        set party_pass_info ""
        if {$course_id == 12266} {
            if {$party_pass_interested_p eq "t"} {
                set party_pass_info ", interested in Party Pass"
            } else {
                set party_pass_info ", not interested in Party Pass"
            }
        }
        append registration_info "<b>Pass:</b> $course_name$party_pass_info <br/>"

    }

    if {[exists_and_not_null event_participant_type_id]} {
        set dance_role [im_name_from_id $event_participant_type_id]
        append registration_info "<b>Dance role:</b> $dance_role <br/>" 
    }

    if {[exists_and_not_null event_partners_text]} {
        append registration_info "<b>Partner:</b> $event_partners_text <br/>"
    }

    if {[exists_and_not_null food_choice_id]} {
        append registration_info "<b>Lunch:</b> [im_material_name -material_id $food_choice_id] <br/>"
    }

    set body [lang::message::lookup $locale intranet-cust-bbw.welcome_email_body ""]

    if {$body ne ""} {
    # Send emails
    acs_mail_lite::send \
        -send_immediately \
        -to_addr $email \
        -from_addr $event_email \
        -subject $subject \
        -body $body \
        -mime_type "text/html" \
        -object_id $participant_id \
        -use_sender    
    }
}


ad_proc -public -callback im_event_participant_after_status_change -impl bbw_send_email_notifications {
    {-participant_id:required}
    {-old_status_id ""}
    {-new_status_id ""}
} {
    Send a mail to the participant about status change
} {
    ns_log Debug "EXECUTING im_event_participant_after_confirm CALLBACK bbw_send_email_notifications"

    set body ""
    set event_email [ad_host_administrator]
    
    # Participant (person), Event (project) info query
    db_0or1row get_participant_info "select ep.course as course_id, ep.food_choice, ep.person_id, ep.participant_id, ep.project_id, ep.event_participant_status_id, u.email, u.first_names, u.last_name, u.user_id, ep.food_choice as food_choice_id from im_event_participants ep, cc_users u where u.user_id = ep.person_id and ep.participant_id=:participant_id"
        
    set event_participant_status_name [im_name_from_id $event_participant_status_id]
    set locale [lang::user::locale -user_id $user_id]
    set token [im_generate_auto_login -user_id $user_id]
    set sencha_registration_app_url [parameter::get_from_package_key -package_key "intranet-event-management" -parameter "SenchaRegistrationAppUrl" -default ""]
    set link "$sencha_registration_app_url/?page=waiting-spot&ws_token=$token&ws_user_id=$user_id"
    
    # Check if lottery was run
    set waiting_spot_info ""
    if {[im_dynfield::attribute::exists_p -object_type event_management_events -attribute_name lottery_run_p]} {
        set lottery_run_p [db_string check_lottery "select 1 from event_management_events where project_id=:project_id and lottery_run_p = '1'" -default 0]
        if {$lottery_run_p eq 1} {
            set waiting_spot [event_management_participant_waiting_spot -participant_id $participant_id]
            set waiting_spot_info [lang::message::lookup $locale intranet-cust-bbw.waiting_spot_info ""]
        } 
    }
    
    # We get event_name which is project_name from im_projects 
    set event_name [im_name_from_id $project_id]    
    set subject [lang::message::lookup $locale intranet-cust-bbw.status_change_email_subject "$event_name - your status has changed"]
    
    switch $new_status_id {
        82500 {
            set body [lang::message::lookup $locale intranet-cust-bbw.status_change_email_body_82500 ""]
        }
        82501 {
            set subject [lang::message::lookup $locale intranet-cust-bbw.status_change_email_subject_82501 ""]
            set workshop_or_partypass_name [im_name_from_id $course_id]
            set workshop_or_partypass [db_string get_material "select material_name from im_materials where material_nr=:workshop_or_partypass_name" -default ""]
            set total_price 0
            set prices "<p>"
            set course_price [db_string get_course_price "select price as course_price from im_materials m, im_timesheet_prices p where m.material_id = p.material_id and p.material_id=:course_id" -default ""]
           
            if {$course_price ne ""} {
                append prices "<br/>$workshop_or_partypass: [format "%.2f" $course_price] [im_cost_default_currency]"
                set total_price [expr $total_price + $course_price]
            }
            set food_price [db_string get_course_price "select price as course_price from im_materials m, im_timesheet_prices p where m.material_id = p.material_id and p.material_id=:food_choice" -default ""]
            if {$food_price ne ""} {
                append prices "<br/>Lunch: [format "%.2f" $food_price] [im_cost_default_currency]"
                set total_price [expr $total_price + $food_price]
            }

            if {$total_price > 0} {
                append prices "<p style='border-top:1px solid black;font-weight:bold;'>Total: [format "%.2f" $total_price] [im_cost_default_currency]</p>"
            }
            append prices "</p>"

            set body [lang::message::lookup $locale intranet-cust-bbw.status_change_email_body_82501 ""]
        }
        82502 {
            set subject [lang::message::lookup $locale intranet-cust-bbw.status_change_email_subject_82502 ""]
            set workshop_or_partypass_name [im_name_from_id $course_id]
            set workshop_or_partypass [db_string get_material "select material_name from im_materials where material_nr=:workshop_or_partypass_name" -default ""]
            set food_name [im_material_name -material_id $food_choice_id]
            set total_price 0
            set prices "<p>"
            set course_price [db_string get_course_price "select price as course_price from im_materials m, im_timesheet_prices p where m.material_id = p.material_id and p.material_id=:course_id" -default ""]
            set currency [db_string get_currency "select currency from im_timesheet_prices where material_id =:course_id limit 1" -default ""]
            if {$course_price ne ""} {
                append prices "<br/>$workshop_or_partypass: [format "%.2f" $course_price] $currency"
                set total_price [expr $total_price + $course_price]
            }
            set food_price [db_string get_course_price "select price as course_price from im_materials m, im_timesheet_prices p where m.material_id = p.material_id and p.material_id=:food_choice" -default ""]
            if {$food_price ne ""} {
                append prices "<br/>Lunch($food_name): [format "%.2f" $food_price] $currency"
                set total_price [expr $total_price + $food_price]
            }

            if {$total_price > 0} {
                set total_price_formatted "[format "%.2f" $total_price] $currency"
                append prices "<p style='border-top:1px solid black;font-weight:bold;'>Total Amount: $total_price_formatted</p>"
            }
            append prices "</p>"
            set money_transfer_reference "$participant_id - $first_names - $last_name"
            set paypal_total_price [expr $total_price * 1.05]
            set paypal_total_price "[format "%.2f" $paypal_total_price] $currency"

            set body [lang::message::lookup $locale intranet-cust-bbw.status_change_email_body_82502 ""]
        }
        82504 {
            set subject [lang::message::lookup $locale intranet-cust-bbw.status_change_email_subject_82504 ""]
            set body [lang::message::lookup $locale intranet-cust-bbw.status_change_email_body_82504 ""]
        }
        82506 {
            set subject [lang::message::lookup $locale intranet-cust-bbw.status_change_email_subject_82506 ""]
            set body [lang::message::lookup $locale intranet-cust-bbw.status_change_email_body_82506 ""]
        }
        default {
            set body ""
        }
    }
    
    if {$body ne ""} {
    
        # Send emails
        acs_mail_lite::send \
            -send_immediately \
            -to_addr $email \
            -from_addr $event_email \
            -subject $subject \
            -body $body \
            -mime_type "text/html" \
            -object_id $participant_id \
            -use_sender
    
    }
    
}


ad_proc -public -callback im_event_participant_after_status_change -impl zz_bbw_confirm_partner {
    {-participant_id:required}
    {-old_status_id ""}
    {-new_status_id ""}
} {
    Send a mail to the participant about status change
} {
    ns_log Debug "EXECUTING im_event_participant_after_confirm CALLBACK bbw_confirm_partner"

   if {$new_status_id eq [event_management::status::confirmed]} {
       
       set waiting_list_status_id [event_management::status::waiting_list]
       # Get partner participant_id
       set partner_participant_id [db_string get_partner_participant_id "select participant_id from im_event_participants ep where ep.event_participant_status_id =:waiting_list_status_id and participant_id in(select event_partner_id from im_event_partners where participant_id=:participant_id and event_partner_mutual_p = 't')" -default ""]

       ns_log Notice "Trying to confirm the partner - $partner_participant_id - for $participant_id"
       if {$partner_participant_id ne ""} {
           event_management_update_participation_status -participant_id $partner_participant_id -status_id [event_management::status::confirmed]
       }
   
   }
    
}
